package com.sda.training.spring.notepad.currency;

import org.springframework.web.bind.annotation.GetMapping;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class EmotionRequest {
	private String text;
}
