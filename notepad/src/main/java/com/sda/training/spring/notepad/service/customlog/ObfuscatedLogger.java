package com.sda.training.spring.notepad.service.customlog;


import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Component
//@Profile("production")
@Profile("!dev")
@Slf4j
public class ObfuscatedLogger implements CustomLogger{
	@Override
	public void log(String msg, String param) {
		log.info("{}, {}", msg, "***********");
	}
}
