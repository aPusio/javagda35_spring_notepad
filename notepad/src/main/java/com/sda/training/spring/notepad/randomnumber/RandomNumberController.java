package com.sda.training.spring.notepad.randomnumber;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/random")
@RequiredArgsConstructor
public class RandomNumberController {
	private final SingletonNumberGenerator singletonNumberGenerator;
	private final RequestNumberGenerator requestNumberGenerator;

	@GetMapping("/singleton")
	//4
	//4
	//4
	public Integer singletonRandom(){
		return singletonNumberGenerator.generate();
	}

	//4
	//8
	//2
	@GetMapping("/request")
	public Integer requestRandom(){
		return requestNumberGenerator.generate();
	}
}
