package com.sda.training.spring.notepad.security;

import java.util.Optional;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import com.sda.training.spring.notepad.adapter.UserDetailsAdapter;
import com.sda.training.spring.notepad.model.CustomUser;
import com.sda.training.spring.notepad.repository.UserRepository;

import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor
public class CustomUserDetailService implements UserDetailsService {
	private final UserRepository userRepository;

	@Override
	public UserDetails loadUserByUsername(String id) throws UsernameNotFoundException {
		Optional<CustomUser> userFromDb = userRepository.findById(id);
		CustomUser user = userFromDb.orElseThrow(() -> new RuntimeException("USER NOT FOUND"));
		return new UserDetailsAdapter(user);
	}
}
