package com.sda.training.spring.notepad.service;

public class NoteNotFoundException extends RuntimeException{
	public NoteNotFoundException(String message) {
		super(message);
	}
}
