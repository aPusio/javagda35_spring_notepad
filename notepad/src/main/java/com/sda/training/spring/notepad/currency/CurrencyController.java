package com.sda.training.spring.notepad.currency;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("/currency")
public class CurrencyController {

	@GetMapping
	public Currency callForCurrency(){
		RestTemplate restTemplate = new RestTemplate();
		Currency currency = restTemplate.getForObject(
			"https://api.exchangeratesapi.io/latest?base=PLN",
			Currency.class);
		return currency;
	}

	@GetMapping("/emotion")
	public EmotionResponse callForEmotion(){
		EmotionRequest emotionRequest = new EmotionRequest();

//		emotionRequest.setText("note.getContent()");
		emotionRequest.setText("I love you");
		RestTemplate restTemplate = new RestTemplate();
		EmotionResponse emotionResponse = restTemplate.postForObject(
			"https://sentim-api.herokuapp.com/api/v1/",
			emotionRequest,
			EmotionResponse.class);

		return emotionResponse;
	}
}
