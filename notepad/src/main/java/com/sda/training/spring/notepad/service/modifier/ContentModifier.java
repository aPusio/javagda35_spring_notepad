package com.sda.training.spring.notepad.service.modifier;

public interface ContentModifier {
	String execute(String content);
}
