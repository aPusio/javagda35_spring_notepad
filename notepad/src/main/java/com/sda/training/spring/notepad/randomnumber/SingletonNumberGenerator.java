package com.sda.training.spring.notepad.randomnumber;

import java.util.Random;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
//@Scope("singleton")
public class SingletonNumberGenerator {
	private final Integer random;

	public SingletonNumberGenerator() {
		random = new Random().nextInt();
	}

	public Integer generate(){
		return random;
	}
}
