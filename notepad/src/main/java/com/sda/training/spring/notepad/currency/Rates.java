package com.sda.training.spring.notepad.currency;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Rates {
	@JsonProperty("CHF")
	private float chf;
	@JsonProperty("EUR")
	private String eur;
	@JsonProperty("PLN")
	private Double pln;
}
