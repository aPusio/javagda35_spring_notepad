package com.sda.training.spring.notepad.service;

import java.util.Optional;

import javax.swing.text.html.Option;

import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.sda.training.spring.notepad.currency.EmotionRequest;
import com.sda.training.spring.notepad.currency.EmotionResponse;
import com.sda.training.spring.notepad.currency.Result;

@Component
public class EmotionService {

	public String callForEmotion(String text){
		EmotionRequest emotionRequest = new EmotionRequest();

		emotionRequest.setText(text);
		RestTemplate restTemplate = new RestTemplate();
		EmotionResponse emotionResponse = restTemplate.postForObject(
			"https://sentim-api.herokuapp.com/api/v1/",
			emotionRequest,
			EmotionResponse.class);

//		emotionResponse.getResult().getType();
		return Optional.ofNullable(emotionResponse)
			.map(EmotionResponse::getResult)
			.map(Result::getType)
			.orElse("unknown");
	}
}
