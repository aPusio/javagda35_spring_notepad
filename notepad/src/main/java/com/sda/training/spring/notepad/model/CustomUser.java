package com.sda.training.spring.notepad.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotEmpty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CustomUser {
	@Id
	private String userName;

	@NotEmpty
	private String password;

	@NotEmpty
	private String role;
}
