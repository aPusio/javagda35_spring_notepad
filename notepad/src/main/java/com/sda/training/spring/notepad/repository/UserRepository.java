package com.sda.training.spring.notepad.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sda.training.spring.notepad.model.CustomUser;

@Repository
public interface UserRepository extends JpaRepository<CustomUser, String> {
}
