package com.sda.training.spring.notepad.currency;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Currency {
	private Rates rates;
	private String base;
	private String date;
}
