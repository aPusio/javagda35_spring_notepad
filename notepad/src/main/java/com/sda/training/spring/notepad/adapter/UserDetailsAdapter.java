package com.sda.training.spring.notepad.adapter;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.sda.training.spring.notepad.model.CustomUser;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class UserDetailsAdapter implements UserDetails {
	private final CustomUser customUser;

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		SimpleGrantedAuthority grantedAuthority = new SimpleGrantedAuthority(customUser.getRole());
		return Collections.singletonList(grantedAuthority);
	}

	@Override
	public String getPassword() {
		return customUser.getPassword();
	}

	@Override
	public String getUsername() {
		return customUser.getUserName();
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}
}
