package com.sda.training.spring.notepad.service.modifier;

import org.springframework.stereotype.Component;

@Component
public class GrammarCorrector implements ContentModifier{
	//MAP im -> I'm
	public String execute(String content){
		return content;
	}
}
