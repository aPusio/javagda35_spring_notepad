package com.sda.training.spring.notepad.service;

import java.util.List;
import java.util.Optional;

import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;

import com.sda.training.spring.notepad.model.Note;
import com.sda.training.spring.notepad.repository.NoteRepository;
import com.sda.training.spring.notepad.service.customlog.CustomLogger;
import com.sda.training.spring.notepad.service.modifier.ContentModifier;

import lombok.RequiredArgsConstructor;

//URL
//>
//CONTROLLER
//>
//SERVICE
//>
//REPOSITORY
//>
//DB


@Service
@RequiredArgsConstructor
public class NoteService {
	private final NoteRepository noteRepository;
//	private final FrameAppender frameAppender;
	private final List<ContentModifier> modifiers;
	private final CustomLogger customLogger;
	private final EmotionService emotionService;

	@Secured("ROLE_ADMIN")
	public Note save(Note note){
		customLogger.log("trying to save", note.getTitle());
		if (note.getId() != null && noteRepository.findById(note.getId()).isPresent()){
			throw new NoteCreationException("Cannot save object with existing id: " + note.getId());
		}
		//post do tamtej strony
		//notSetEmotion
		note.setContent(modify(note));
		note.setEmotion(emotionService.callForEmotion(note.getContent()));
//		note.setContent(frameAppender.execute(note.getContent()));
		return noteRepository.save(note);
	}

	private String modify(Note note) {
		String finalContent = note.getContent();
		for (ContentModifier modifier : modifiers) {
			finalContent = modifier.execute(finalContent);
		}
		return finalContent;
	}

	public List<Note> findAll(){
		return noteRepository.findAll();
	}

	public Optional<Note> update(Long id, Note note){
		Optional<Note> byId = noteRepository.findById(id);
		if(byId.isEmpty()){
			return byId;
		}
		Note noteFromDb = byId.get();
		noteFromDb.setTitle(note.getTitle());
		noteFromDb.setContent(note.getContent());
		noteFromDb.setUpdateTime(note.getUpdateTime());
		noteFromDb.setCreationTime(note.getCreationTime());

		note.setContent(modify(note));
		return Optional.of(noteRepository.save(noteFromDb));
	}

	public Note findById(Long id){
		Optional<Note> note = noteRepository.findById(id);
		if(!note.isPresent()){
			throw new NoteNotFoundException("Note " + id + " was not found");
		}
		return note.get();
	}

	public Optional<Note> delete(Long id){
		Optional<Note> byId = noteRepository.findById(id);
		if(byId.isEmpty()){
			return byId;
		}
		noteRepository.deleteById(id);
		return byId;
	}

	public List<Note> findByTitleAndContentContains(String title, String content){
		return noteRepository.findByTitleAndContentContains(title, content);
	}

}
