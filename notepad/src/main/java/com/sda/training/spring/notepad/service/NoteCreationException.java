package com.sda.training.spring.notepad.service;


public class NoteCreationException extends RuntimeException {
	public NoteCreationException(String message) {
		super(message);
	}
}
