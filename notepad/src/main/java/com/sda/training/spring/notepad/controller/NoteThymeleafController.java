package com.sda.training.spring.notepad.controller;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.sda.training.spring.notepad.model.Note;
import com.sda.training.spring.notepad.service.NoteService;

import lombok.RequiredArgsConstructor;

@Controller
@RequestMapping("/frontend")
@RequiredArgsConstructor
public class NoteThymeleafController {

	private final NoteService noteService;

	@GetMapping("/hello")
	public String helloWorld(ModelMap modelMap){
		modelMap.addAttribute("greetings", "HELLO FROM CONTROLLER - modelMap!");
		return "hello";
	}

	@GetMapping("/hello2")
	public ModelAndView helloWorld2(){
		ModelAndView modelAndView = new ModelAndView("hello");
		modelAndView.addObject("greetings", "HELLO FROM CONTROLLER! - ModelAndView");
		return modelAndView;
	}

	@GetMapping
	public String index(ModelMap modelMap){
		modelMap.addAttribute("hello", "HELLO WORLD!");
		modelMap.addAttribute("hellodescription", "hello, thanks for visiting our site 😀");
		return "index";
	}
	@GetMapping("/notes")
	public String notes(ModelMap modelMap){
		List<Note> all = noteService.findAll();
		modelMap.addAttribute("notes", all);
		return "notes";
	}

	//TBD
//	@GetMapping("/hello3")
//	@ModelAttribute("greetings")
//	public String helloWorld3(){
//		return "HELLO FROM CONTROLLER - ModelAttributAnnotation";
//	}

}
