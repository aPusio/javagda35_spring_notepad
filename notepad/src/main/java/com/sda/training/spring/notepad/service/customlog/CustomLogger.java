package com.sda.training.spring.notepad.service.customlog;

public interface CustomLogger {
	public void log(String msg, String param);
}
