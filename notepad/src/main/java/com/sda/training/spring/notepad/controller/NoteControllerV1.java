package com.sda.training.spring.notepad.controller;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.sda.training.spring.notepad.exception.ApiError;
import com.sda.training.spring.notepad.model.Note;
import com.sda.training.spring.notepad.service.NoteCreationException;
import com.sda.training.spring.notepad.service.NoteService;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/api/notes")
public class NoteControllerV1 {
	private final NoteService noteService;

	@RequestMapping(value = "/helloworld", method = RequestMethod.GET)
	public String helloWorld(){
		return "hello World!";
	}

	//TEN DLA USERA
	@GetMapping("/{id}")
	@ResponseStatus(HttpStatus.OK)
	public Note getById(@PathVariable Long id){
		return noteService.findById(id);
	}

	//TEN DLA ADMINA
	@GetMapping
	public List<Note> getAllNotes(){
		return noteService.findAll();
	}

	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public Note save(@Valid @RequestBody Note note){
		return noteService.save(note);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Note> delete(@PathVariable Long id){
		Note byId = noteService.findById(id);
		noteService.delete(id);
		return ResponseEntity.ok(byId);
	}

	@PutMapping("/{id}")
	public ResponseEntity<Note> update(@PathVariable Long id, @RequestBody Note note){
//		Optional<Note> byId = noteRepository.findById(id);
//		if(byId.isEmpty()){
//			return ResponseEntity.status(HttpStatus.NOT_FOUND)
//					   .build();
//		}
//		Note noteFromDb = byId.get();
//		noteFromDb.setTitle(note.getTitle());
//		noteFromDb.setContent(note.getContent());
//		noteFromDb.setUpdateTime(note.getUpdateTime());
//		noteFromDb.setCreationTime(note.getCreationTime());
//
//		noteFromDb.setContent(frameAppender.execute(noteFromDb.getContent()));
//		return ResponseEntity.ok(noteRepository.save(noteFromDb));

		Optional<Note> update = noteService.update(id, note);
		if(update.isEmpty()){
			return ResponseEntity.status(HttpStatus.NOT_FOUND)
					   .build();
		}
		return ResponseEntity.ok(update.get());
	}

	@GetMapping("/filter")
	public List<Note> filter(@RequestParam(required = false) String title, @RequestParam String content){
		return noteService.findByTitleAndContentContains(title, content);
	}

//	@ExceptionHandler({NoteCreationException.class, NoteNotFoundException.class})
	@ExceptionHandler(NoteCreationException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public ApiError noteCreationExceptionHandler(RuntimeException runtimeException){
		return new ApiError(UUID.randomUUID(), runtimeException.getMessage(), LocalDateTime.now());
	}

	@ExceptionHandler(MethodArgumentNotValidException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public ApiError constraintExceptionHandler(MethodArgumentNotValidException exception){
		return new ApiError(
			UUID.randomUUID(),
			exception.getParameter() + " : " +exception.getMessage(),
			LocalDateTime.now());
	}
}
