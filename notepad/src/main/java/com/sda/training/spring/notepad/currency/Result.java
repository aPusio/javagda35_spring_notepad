package com.sda.training.spring.notepad.currency;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Result {
	private float polarity;
	private String type;
}
