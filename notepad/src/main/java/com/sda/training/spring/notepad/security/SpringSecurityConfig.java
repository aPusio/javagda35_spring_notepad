package com.sda.training.spring.notepad.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.crypto.password.StandardPasswordEncoder;

import lombok.RequiredArgsConstructor;

@Configuration
@EnableGlobalMethodSecurity(securedEnabled = true)
@RequiredArgsConstructor
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {
	private final CustomUserDetailService customUserDetailService;

	@Override
	protected UserDetailsService userDetailsService() {
		return customUserDetailService;
	}

//	@Override
//	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//		auth.inMemoryAuthentication()
//			.withUser("user1")
//			.password("36b85ddcfdc1726312cde558915dd7ec6aa36a403eac351fa3ed6712218014fca4d1d0fff5db55d1")
//			.roles(Roles.USR.getFullName())
//			.and()
//			.withUser("admin")
//			.password("36b85ddcfdc1726312cde558915dd7ec6aa36a403eac351fa3ed6712218014fca4d1d0fff5db55d1")
//			.roles(Roles.ADM.getFullName());
//	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests()
			.antMatchers(HttpMethod.GET, "/v1/api/notes/helloworld")
			.permitAll()
			.antMatchers(HttpMethod.GET,"/v1/api/notes/*")
			.hasRole(Roles.USR.getFullName())
			.antMatchers(HttpMethod.GET, "/v1/api/notes")
			.hasRole(Roles.ADM.getFullName())
//			.antMatchers(HttpMethod.POST, "/v1/api/notes")
//			.permitAll()
			.anyRequest()
			.authenticated()
			.and()
			.formLogin()
			.and()
			.logout()
			.and()
			.httpBasic()
			//make h2-console great again
			.and()
			.headers()
			.frameOptions()
			.disable()
			.and()
			.csrf()
			.disable();
	}

	@Bean
	public PasswordEncoder initPasswordEncoder(){
		//return NoOpPasswordEncoder.getInstance();
		StandardPasswordEncoder standardPasswordEncoder = new StandardPasswordEncoder();
		return standardPasswordEncoder;
	}
}
