package com.sda.training.spring.notepad.exception;

import java.time.LocalDateTime;
import java.util.UUID;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.sda.training.spring.notepad.service.NoteNotFoundException;

@RestControllerAdvice
public class NoteExceptionHandler {

	@ExceptionHandler(NoteNotFoundException.class)
	public ApiError noteNotFoundHandler(RuntimeException runtimeException){
		return new ApiError(UUID.randomUUID(), runtimeException.getMessage(), LocalDateTime.now());
	}
}
