package com.sda.training.spring.notepad;


import java.time.LocalDateTime;

import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import com.sda.training.spring.notepad.model.CustomUser;
import com.sda.training.spring.notepad.model.Note;
import com.sda.training.spring.notepad.repository.NoteRepository;
import com.sda.training.spring.notepad.repository.UserRepository;

import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor
public class Invoker implements CommandLineRunner {
	private final NoteRepository noteRepository;
	private final PasswordEncoder passwordEncoder;
	private final UserRepository userRepository;

	@Override
	public void run(String... args) throws Exception {
		Note sampleNote = new Note();
		sampleNote.setTitle("Sample title");
		sampleNote.setContent("Lorem ipsum ...");
		sampleNote.setCreationTime(LocalDateTime.now());
		sampleNote.setUpdateTime(LocalDateTime.now());

		noteRepository.save(sampleNote);
//		System.out.println("*******");
//		System.out.println(passwordEncoder.encode("hmm"));
//		System.out.println("*******");

		CustomUser customUser = new CustomUser(
			"apusio",
			passwordEncoder.encode("haha"),
			"ADMIN");
		userRepository.save(customUser);
	}
}
