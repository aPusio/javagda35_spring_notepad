package com.sda.training.spring.notepad.exception;

import java.time.LocalDateTime;
import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class ApiError {
	private UUID id;
	private String message;
	private LocalDateTime timestamp;
}
