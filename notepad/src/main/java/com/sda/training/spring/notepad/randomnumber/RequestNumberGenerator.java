package com.sda.training.spring.notepad.randomnumber;

import java.util.Random;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.ApplicationScope;
import org.springframework.web.context.annotation.RequestScope;

@Component
@RequestScope
//@SessionScope
//@ApplicationScope
public class RequestNumberGenerator {
	private final Integer random;

	public RequestNumberGenerator() {
		random = new Random().nextInt();
	}

	public Integer generate(){
		return random;
	}
}
